+++
title = "Cherif, R. (2019) The return of the policy that shall not be named: Principles of industrial policy"
date = 2023-02-13T15:45:00+00:00
draft = false
+++

It is not common for countries to gain high-income status in few generations. If there is a policy that sets itself to help it, it is industrial policy.

This paper follows the work of Friedrich List and Ha-Joon Chang, among others. It advances that there are similarities across the Asian Miracles that should not be disregarded. It also finds that the story of the development of western countries like the US and Germany hold useful - and, for some, surprising - lessons.

Luck is studied, so that its effects can be disentangled from growth-determinants-increasing policies. The analysis is further cautioned by attention paid to tail, and not average, effects.

Cherif et al pertain to be the first to describe the key principles of industrial policy. They arrive at three points that are central to what they call "Technology and Innovation Policy":

1.  fixing market failures, important for advent of sophisticated industries;
2.  export orientation, and not import substitution industrialization;
3.  domestic and international competition, with strict accountability.

Of course, the success of these policies varies depending on the technological leap that is attempted. Diversification in the tradable sector also plays a role.

True Industrial Policy depends on both private and public action. The State shall affect the allocation of capital and labor that would not be otherwise possible. The private sector, via its market signals and investment decisions, has a role to play. When the latter takes the lead, the former might go where otherwise it wouldn't.

The authors thus postulate the existence of three conditions Ambition, Accountability, and Adaptability necessary for the implementation of TIP. The triple A conditions constitute the "leading hand of the state", as "[t]he state has to set the level of ambition of its goal, and then implement the right policies while imposing accountability and being able to adapt fast as conditions change."

A leading hand can push one of three gears:

1.  moonshot, pursuing an economic "miracle";
2.  leapfrog, providing decent growth up to a middle-income status; and
3.  snail crawl, achieving relatively lower growth.

---

What has the debate on the Asian miracles found?

It is broadly assumed (e.g., by the World Bank) that some policies were instrumental for the successful economic change, but the role of industrial policy is often questioned. Macroeconomic stability and liberalized trade are one thing, support for specific industries is another. In fact, the accepted view was that "export-push strategies using standard growth policies" - a snail-crawl approach - hold the most promise.

The judgment is split. In the case of Hong Kong, for example, some say that free-market policies were the true culprit, and not interventionist industrial policy. This position goes against the government's own words of deploying "soft" industrial policy, mostly found in the promotion of technology and innovation. More broadly, the research posits that:

-   economic miracles were caused by standard growth factors;
-   economic miracles were statistical anomalies;
-   investment in physical and human capital resulted in "perspiration", seen in higher working hours;
-   investment in physical and human capital resulted in "inspiration", seen in high productivity rates;
-   change was caused by culture and geography traits, thus unrepeatable; and
-   change was caused by a developmental state, thus repeatable.

Since the turn of the century, authors like Dani Rodrik and Stiglitz have suggested that certain policies did indeed induce a structural transformation of these Asian developing economies. This way, they coalesce around the notion of the developmental state and its policies of 1) high investment rates, 2) promotion of sectors via state-owned enterprises, 3) directing the entrance of private firms into some sectors they would not otherwise enter, and 4) strong export orientation.

Regardless of findings, the debate has been confrontational and undertaken under a veil of skepticism by many. While some say that industrial policy has returned, others respond that it never went anywhere.

Recent development in this area can be glanced at in the next excerpt:

> Chang (2009, 2013) proposes to go beyond the confrontation and see what we can learn from this debate. He stresses the importance of **export orientation as a common ground**. He further argues that even such crucial problems as the lack of capabilities and political economy issues that tend to translate into advice for inaction, should not let "the best become the enemy of the good." In the same vein of surmounting the ideological confrontation, Benhassine and Raballand (2009) suggest that **industrial policy may be most needed in low-income countries with undiversified industrial base**. They argue that even if conditions for interventions are very weak, examples show that interventions can still succeed. Harrison and Rodriguez-Clare (2010) discuss theoretical and empirical work on industrial policy and suggest that there is an important role for a **"soft" industrial policy, in which government and the private sector collaborate on interventions to increase productivity**. IDB (2014) argues along the same lines based on the Latin American experience. Justin Lin (2012) suggests that **the state could provide a facilitating role in identifying and helping develop industries around comparative advantage along with markets and firms leading the technological innovation**.
>
> Stiglitz and Greenwald (2014) argue that what separates advanced countries from developing countries is the **gap in knowledge**. They start with Arrow’s (1962) seminal paper “Learning by Doing” and contend that markets will under provide the production and diffusion of knowledge. Closing the knowledge gap would require a well-crafted industrial policy to **encourage learning and create a learning society**. Mazzucato (2013) shows how important **the state’s role is in promoting innovation and growth in advanced countries**.

Lastly, there is the issue of what constitutes industrial policy. Ha-Joon Chang points out that some policies relating to infrastructure and education benefit some industries over others. These could be seen as implicit industrial policy.

---

Development economics has ignored the Asian miracle in two ways, that one could call schools: one uses History to tell a pre-conceived story, the other is based on empirical studies. In each case, they disregard the metamorphoses that these economies went through and simply paint the changes as resulting from hard-work.

The empirical exercise seemed to suggest that accumulation, and not TFP was the central piece.

But still, problems persist.

Portugal is mentioned as an example that the normal story - given by a growth accounting exercise where the solution is accumulating capital - doesn't hold:

> Many countries had high rates of investment over 1970–90, that were in many cases comparable to those in the Asian miracles, while simultaneously displaying much lower average TFP growth rates (see Figure 3). Korea for example had an impressive average investment rate of about 30 percent between 1970 and 1990 but this was not exceptional. **A diverse group of countries had similar or greater investment rates such as China, Iran, Jordan, Portugal, and Saudi Arabia over the same period and average TFP growth was negative in all these countries**. Many of these countries did not succeed in achieving sustained high growth despite their high rates of accumulation of both physical and human capital (see Cherif, Hasanov and Zhu 2016 for some examples). Something fundamental must distinguish the experience of these countries over that period from the Asian miracles beyond the rate of accumulation of capital.

TFP growth seems to have been sustained longer than in other countries.

There is a third way in which the history of the development of these countries is ignored: methodological grounds. In cross-country growth regressions, the Asian miracle countries were considered outliers, and thus removed from the samples.
