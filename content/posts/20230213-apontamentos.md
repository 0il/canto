+++
title = "apontamentos de segunda 2"
date = 2023-02-13T14:49:00+00:00
draft = false
+++

1.  [FT](https://www.ft.com/content/48f174fb-6c44-4ce4-862c-08ae3ea48b22) pede às petrolíferas que orientem lucros para a transição verde:

    > Energy security should, however, not be used as an excuse to slow down on green initiatives. The best way to support energy supply in the long run is by focusing on renewable power sources and decarbonisation. Governments globally can provide more sweeteners: committing to spending on clean tech, incentivising green investments through the tax system, and ensuring planning regulations do not hinder renewable projects. Disincentivising fossil fuel focus over the medium term and broadening carbon pricing mechanisms are also important sticks. And ultimately, Big Oil CEOs and their shareholders need to wake up fast to the existential risk of leaning too heavily on the declining petroleum economy.
2.  Imaginação pia:

    > In fact, the very reason why nearly all the famous statues of the ancient world perished was that after the victory of Christianity it was considered a pious duty to smash any statue of the heathen gods. The sculptures in our museums are, for the most part, only secondhand copies made in Roman times for travellers and collectors as souvenirs, and as decorations for gardens or public baths. We must be very grateful for these copies, because they give us at least a faint idea of the famous masterpieces of Greek art; but unless we use our imagination these weak imitations can also do much harm. (<a href="#citeproc_bib_item_1">Gombrich 1995, 84</a>)
3.  Perspetiva estilística:

    > The fact is that even Hellenistic artists did not know what we call the laws of perspective. The famous avenue of poplars, which recedes to a vanishing point and which so many of 11s drew at school, was not then a standard task. Artists drew distant things small, and near or important things large, but the law of regular diminution of objects as they become more distant, the fixed framework which we can represent a view, was not adopted by classical antiquity. Indeed, it took more than another thousand years before it was applied. Thus even the latest, freest and most confident works of ancient art still preserve at least a remnant of the principle which we discussed in our description of Egyptian painting. Even here, knowledge of the characteristic outline of individual objects counts for as much as the actual impression received through the eye. We have long recognized that this quality is not a fault in works of art, to be regretted and looked down upon, but that it is possible to achieve artistic perfection within any style.  (<a href="#citeproc_bib_item_1">Gombrich 1995, 115</a>)
4.  [Leituras clusterizadas](https://marginalrevolution.com/marginalrevolution/2023/02/chatgpt-and-reading-in-clusters.html).
5.  Saberei que sei que [irei](https://cful.letras.ulisboa.pt/events/introduction-to-epistemic-logic-curso-livre/)?

    > Pretende-se, em particular, que fiquem familiarizados com a discussão acerca do axioma “Se o agente sabe que p, então o agente sabe que sabe que p” (conhecido na literatura como “princípio KK”).
6.  Laocoön Grego: [![](/canto/20220213-laocoon.jpg)](https://en.wikipedia.org/wiki/File:El_Greco_(Domenikos_Theotokopoulos)_-_Laoco%C3%B6n_-_Google_Art_Project.jpg)
7.  Xs obreirxs acordadxs, via [Tyler](https://marginalrevolution.com/marginalrevolution/2023/02/why-do-companies-go-woke.html):

    > “Woke” companies are those that are committed to socially progressive causes, with a particular focus on diversity, equity, and inclusion as these terms are understood through the lens of critical theory. There is little evidence of systematic support for woke ideas among executives and the population at large, and going woke does not appear to improve company performance. Why, then are so many firms embracing woke policies and attitudes? We suggest that going woke is an emergent strategy that is largely shaped by middle managers rather than owners, top managers, or employees. We build on theories from agency theory, institutional theory, and intra-organizational ecology to argue that wokeness arises from middle managers and support personnel using their delegated responsibility and specialist status to engage in woke internal advocacy, which may increase their influence and job security. Broader social and cultural trends tend to reinforce this process. We discuss implications for organizational behavior and performance including perceived corporate hypocrisy (“woke-washing”), the potential loss of creativity from restricting viewpoint diversity, and the need for companies to keep up with a constantly changing cultural landscape.
8.  Conjeturas do [mal](https://twitter.com/0x49fa98/status/1621970523230969856).

## References

<style>.csl-entry{text-indent: -1.5em; margin-left: 1.5em;}</style><div class="csl-bib-body">
  <div class="csl-entry"><a id="citeproc_bib_item_1"></a>Gombrich, Leonie. 1995. <i>The story of art</i>. 16th ed. Phaidon Press.</div>
</div>
