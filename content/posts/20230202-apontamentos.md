+++
title = "apontamentos de quinta"
date = 2023-02-03T01:51:00+00:00
draft = false
+++

1.  [New Yorker](https://www.newyorker.com/magazine/2023/02/06/when-americans-lost-faith-in-the-news) sobre o jornalismo:

    > And the business is all about the eyeballs. When ratings drop, and with them advertising revenues, correspondents change, anchors change, coverage changes. News, especially but not only cable news, is curated for an audience. So, obviously, is the information published on social media, where the algorithm selects for the audience’s political preferences. It is hard to be “objective” and sell news at the same time.
    >
    > What is the track record of the press since Lippmann’s day? In “City of Newsmen: Public Lies and Professional Secrets in Cold War Washington” (Chicago), Kathryn J. McGarr weighs the performance of the Washington press corps during the first decades of the Cold War. She shows, by examining archived correspondence, that reporters in Washington knew perfectly well that Administrations were misleading them about national-security matters—about whether the United States was flying spy planes over the Soviet Union, for example, or training exiles to invade Cuba and depose Fidel Castro. To the extent that there was an agenda concealed by official claims of “containing Communist expansion”—to the extent that Middle East policy was designed to preserve Western access to oil fields, or that Central American policy was designed to make the region safe for United Fruit—reporters were not fooled.
    >
    > So why didn’t they report what they knew? McGarr, a historian at the University of Wisconsin-Madison, thinks it’s because the people who covered Washington for the wire services and the major dailies had an ideology. They were liberal internationalists. Until the United States intervened militarily in Vietnam—the Marines waded ashore there in 1965—that was the ideology of American élites. Like the government, and like the leaders of philanthropies such as the Ford Foundation and cultural institutions such as the Museum of Modern Art, newspaper people believed in what they saw as the central mission of Cold War policy: the defense of the North Atlantic community of nations. They supported policies that protected and promoted the liberal values in the name of which the United States had gone to war against Hitler.
    >
    > ---
    >
    > The medium got the message. After Chicago, as Hodgson explains, coverage of political unrest, the civil-rights movement, and the war was vastly reduced. By the end of 1970, people had almost forgotten about Vietnam (although Americans continued to die there for five more years), partly because they were seeing and reading much less about it. The networks understood that most viewers did not want to see images of wounded soldiers or antiwar protesters or inner-city rioters. They also understood that the government held, as it always had, the regulatory hammer.
    >
    > ---
    >
    > Hendershot’s argument seems to be missing a step, though. If the coverage in Chicago was (to borrow, with tongs, the slogan of Fox News) “fair and balanced,” why did the public feel differently? It would make sense for the press to lose credibility if it had delivered biased or sensationalized news. But it hadn’t. It had barely covered the protesters at all. Something else was going on.
    >
    > That something was the war. Vietnam was the beginning of our present condition of polarization, and one of the features of polarization is that there is no such thing as objectivity or impartiality anymore. In a polarized polity, either you’re with us or you’re against us. You can’t be disinterested, because everyone knows that disinterestedness is a façade. Viewers in 1968 didn’t want fair and balanced. They wanted the press to condemn kids with long hair giving cops the finger.
    >
    > ---
    >
    > The power of the press, such as it is, is like the power of academic scholars, scientific researchers, and Supreme Court Justices. It is not backed by force. It rests on faith: the belief that these are groups of people dedicated to pursuing the truth without fear or favor. Once they disclaim that function, they will be perceived in the way everyone else is now perceived, as spinning for gain or status.
2.  Muito interessante:

    > There are people, for instance, who have picked up the simple points I have tried to make in this chapter, and who understand that there are great works of art which have none of the obvious qualities of beauty of expression or correct draughtsmanship, but who become so proud of their knowledge that they pretend to like only those works which are neither beautiful nor correctly drawn. They are always haunted by the fear that they might be considered uneducated if they confessed to liking a work which seems too obviously pleasant or moving. They end by being snobs who lose their true enjoyment of art and who call everything ‘very interesting’ which they really find somewhat repulsive. I should hate to be responsible for any similar misunderstanding. I would rather not be believed at all than be believed in such an uncritical way. (<a href="#citeproc_bib_item_1">Gombrich 1995, 37</a>)
3.  Meses eleitorais:

    > And so this calendar became lunisolar.
    >
    > But this system was open to abuse, because Roman officials served annual terms.
    >
    > The priests in charge of adding those intercalary months were also politicians, and they often made years shorter or longer depending on whether their enemies or allies were in power. ([@culturaltutor](https://nitter.net/culturaltutor/status/1620810910322524160))
4.  A história e o legado do [meme mais antigo](https://en.wikipedia.org/wiki/Kilroy_was_here).

## References

<style>.csl-entry{text-indent: -1.5em; margin-left: 1.5em;}</style><div class="csl-bib-body">
  <div class="csl-entry"><a id="citeproc_bib_item_1"></a>Gombrich, Leonie. 1995. <i>The story of art</i>. 16th ed. Phaidon Press.</div>
</div>
